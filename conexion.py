import socket
import threading

from enviar import *

class Conexion(threading.Thread):

	def __init__(self, enviar, recibir, dDispositivos):
		threading.Thread.__init__(self)
		self.enviar = enviar
		self.recibir = recibir
		self.dDispositivos = dDispositivos

	def run(self):
		host = '192.168.1.222'
		port = 4444

		client = socket.socket()
		client.connect((host,port))

		x = "Interfaz Conectada"
		client.send(x.encode())

		conexionEnvio = Enviar(self.enviar, client)
		conexionEnvio.start()


		while True:
			try:
				data = client.recv(1024).decode()
				self.recibir.put(data)
				print ('Received: ' + data)

				if "DIS" == data[0:3]:
					dis = data[4:]

					self.dDispositivos.put(dis)
					self.enviar.put("ACTUALIZADO")

			except KeyboardInterrupt as k:
				print("Shutting down.")
				client.close()
				break

