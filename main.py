from domo import *
from conexion import *
import queue, time

enviar = queue.Queue()
recibir = queue.Queue()
dDispositivos = queue.Queue()
eventos = list()
comandos = {}
dispositivosIp = {}


if __name__ == '__main__':
	
	app = QApplication(sys.argv)



	pixmap = QPixmap('images/inicio.png')
	splashscreen = QSplashScreen(pixmap)

	splashscreen.show()
	time.sleep(1)
	
	domo = Domo(enviar, dDispositivos, eventos, comandos, dispositivosIp, recibir)

	splashscreen.finish(domo)

	conexion = Conexion(enviar, recibir, dDispositivos)
	conexion.start()
	

	
	sys.exit(app.exec_())