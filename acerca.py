from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import sys, time, random


class Acerca(QWidget):
	def __init__(self):
		super().__init__()
		self.initUI()

	def initUI(self):

		self.imagen = QPixmap('images/logoDomo.png')
		self.labelImg = QLabel(self)

		self.labelImg.setPixmap(self.imagen)

		self.labelImg.move(120,10)


		self.uno = QLabel("DOMO", self)
		self.dos = QLabel("Proyecto de Residencia", self)
		self.tres = QLabel("Instituto Tecnológico José Mario Molina Pasquel y Henríquez", self)
		self.cuatro = QLabel("Version 1.0", self)

		self.uno.move(100,60)
		self.dos.move(90,100)
		self.tres.move(7, 155)
		self.cuatro.move(120,170)

		self.uno.setStyleSheet("font-size: 40px; qproperty-alignment: AlignJustify; font-family: Courier New;")

		self.setGeometry(500,150,300,200)
		self.setWindowTitle('Domo')
		self.setWindowIcon(QIcon('images/domo.png'))
		self.show()