import threading
import time

class Play(threading.Thread):

	def __init__(self, eventos, enviar, final, comandosEdit):
		threading.Thread.__init__(self)
		self.eventos = eventos
		self.enviar = enviar
		self.final = final
		self.comandosEdit = comandosEdit

		self.comandos = {}
		self.paused = False
		self.stoped = False

	def run(self):
		for c in self.eventos:
			inicio = c.getInicio()
			fin = c.getFin()
			ip = c.getIp()

			comandoInicio = "SI -d " + ip + " -p 12 -o H"
			comandoFin = "SI -d " + ip + " -p 12 -o L"

			comandoActual = self.comandos.get(inicio)

			if comandoActual == None:
				self.comandos[inicio] = [comandoInicio]
			else:
				self.comandos[inicio].append(comandoInicio)


			comandoActual = self.comandos.get(fin)

			if comandoActual == None:
				self.comandos[fin] = [comandoFin]
			else:
				self.comandos[fin].append(comandoFin)

		for x in range(0,self.final + 1):

			print(x)

			if self.paused == True:
				while self.paused:
					pass

			if self.stoped == True:
				return 0
			
			comando = self.comandos.get(x)
			if comando == None:
				pass
			else:
				for i in comando:						
					actual = ""
					mensaje = "SE ENVIO EL COMANDO: " + i

					actual = self.comandosEdit.toPlainText()
					
					self.comandosEdit.selectAll()
					self.comandosEdit.cut()

					self.comandosEdit.insertPlainText(mensaje + "\n-------------------------------------------\n" + actual)
					self.enviar.put(i)

			time.sleep(1)

		self.stoped = True

		mensaje = "TERMINO EL ENVIO DE COMANDOS"

		actual = self.comandosEdit.toPlainText()

		self.comandosEdit.selectAll()
		self.comandosEdit.cut()

		self.comandosEdit.insertPlainText(mensaje + "\n-------------------------------------------\n" + actual)
		
		return 0


	def pause(self):
		self.paused = True

	def resume(self):
		self.paused = False

	def stop(self):
		self.stoped = True
