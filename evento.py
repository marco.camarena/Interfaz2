class Evento(object):
	def __init__(self, nombreEvento, nombreDispositivo, ip, inicio, fin, intensidad):
		super().__init__()

		self.nombreEvento = nombreEvento
		self.nombreDispositivo = nombreDispositivo
		self.ip = ip
		self.inicio = inicio
		self.fin = fin
		self.intensidad = intensidad

	def getNombreEvento(self):
		return self.nombreEvento

	def getNombreDispositivo(self):
		return self.nombreDispositivo

	def getIp(self):
		return self.ip

	def getInicio(self):
		return self.inicio

	def getFin(self):
		return self.fin

	def getIntensidad(self):
		return self.intensidad

	def cambiarDispositivo(self, nuevoDispositivo):
		self.nombreDispositivo = nuevoDispositivo

	def cambiarIp(self, nuevaIp):
		self.ip = nuevaIp

	def cambiarInicio(self, nuevoInicio):
		self.inicio = nuevoInicio

	def cambiarFin(self, nuevoFin):
		self.fin = nuevoFin

	def cambiarIntensidad(self, nuevaIntensidad):
		self.intensidad = nuevaIntensidad