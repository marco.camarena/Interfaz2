from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import sys, time, random

class RenderArea(QWidget):
	def __init__(self, path, eventos, parent = None):
		super().__init__(parent)

		self.path = path
		self.eventos = eventos
		
		self.setGeometry(0,0,288000,320)

	def actualizarPainter(self):
		self.update()

	def paintEvent(self, event):

		painter = QPainter(self)

		for x in range(1,7200):
			texto = ""
			segundos = x

			t1 = int(segundos) // 3600
			if t1 >= 1:
				segundos = int(segundos) - (t1 * 3600)

			t2 = int(segundos) // 60
			if t2 >= 1:
				segundos = int(segundos) - (t2 * 60)

			t3 = int(segundos)

			t1 = str(t1)
			if t2 < 10:
				t2 = str(t2)
				t2 = "0" + t2
			else:
				t2 = str(t2)
			if t3 < 10:
				t3 = str(t3)
				t3 = "0" + t3
			else:
				t3 = str(t3)

			texto = t1 + ":" + t2 + ":" + t3

			pen = QPen(Qt.gray, 1, Qt.DotLine)
			painter.setPen(pen)
			
			painter.drawLine(x*60, 0, x*60, 310)

			pen = QPen(Qt.black, 1, Qt.SolidLine)
			painter.setPen(pen)

			painter.drawText((x*60)-20, 320, texto)

		dispositivos = {}
		numDisp = 0
		pos = {1:20, 2:100, 3:180, 4:260, 5:340, 6:420, 7:500}
		color = {1:'magenta', 2:'green', 3:'yellow', 4:'cyan', 5:'red', 6:'blue', 7:'gray'}

		pen = QPen(Qt.black, 1, Qt.SolidLine)
		painter.setPen(pen)
	
		for c in self.eventos:
			nombreEven = c.getNombreEvento()
			nombre = c.getNombreDispositivo()
			inicio = c.getInicio()
			fin = c.getFin()

			numDispositivo = dispositivos.get(nombre)

			if numDispositivo == None:
				numDisp = numDisp + 1
				dispositivos[nombre] = numDisp
				numDispositivo = numDisp

			painter.setBrush(QColor(color[numDispositivo]))

			tiempo = fin - inicio
			painter.drawRect(inicio * 60, pos[numDispositivo] , tiempo * 60, 50)
			painter.drawText((inicio*60) + 5, (pos[numDispositivo]) + 20, nombreEven)