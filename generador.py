from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import sys, time, random


class Generador(QWidget):
	def __init__(self):
		super().__init__()
		self.initUI()

		self.nombreDispositivo = ""
		self.tipoDispositivo = ""
		self.ipDispositivo = ""
		self.mac = ""
		self.ipServidor = ""
		self.seleccionTarjeta = ""

		self.fileDialog = QFileDialog()

	def initUI(self):

		self.tipoDispositivoLabel = QLabel("Tipo de Dispositivo", self)
		self.nombreDispositivoLabel = QLabel("Nombre del Dispositivo", self)
		self.ipDispositivoLabel = QLabel("IP del Dispositivo", self)
		self.ipServidorLabel = QLabel("IP del Servidor", self)
		self.macLabel = QLabel("MAC del Dispositivo", self)
		self.nombreRedLabel = QLabel("Nombre red WIFI", self)
		self.contraRedLabel = QLabel("Contraseña WIFI", self)

		self.nombreDispositivoLine = QLineEdit(self)
		self.tipoDispositivoCombo = QComboBox(self)
		self.ipDispositivoLine = QLineEdit(self)##VER SI SE PUEDE PUROS NUMEROS
		self.macLine = QLineEdit(self)
		self.ipServidorLine = QLineEdit(self)##VER SI SE PUEDE PUROS NUMEROS
		self.nombreRedLine = QLineEdit(self)
		self.contraRedLine = QLineEdit(self)

		self.tipoDispositivoCombo.activated[str].connect(self.mostrarVentana)

		self.generarButton = QPushButton("GENERAR", self)
		self.limpiarButton = QPushButton("LIMPIAR", self)

		self.tipoDispositivoLabel.move(20,20)
		self.nombreDispositivoLabel.move(500,500)
		self.ipDispositivoLabel.move(500,500)
		self.ipServidorLabel.move(500,500)
		self.macLabel.move(500,500)
		self.nombreRedLabel.move(500,500)
		self.contraRedLabel.move(500,500)

		self.tipoDispositivoCombo.move(150,20)
		self.nombreDispositivoLine.move(500,500)
		self.ipDispositivoLine.move(500,500)
		self.ipServidorLine.move(500,500)
		self.macLine.move(500,500)
		self.nombreRedLine.move(500,500)
		self.contraRedLine.move(500,500)

		self.limpiarButton.move(100,300)
		self.generarButton.move(200,300)

		self.tipoDispositivoCombo.resize(133,23)

		self.tipoDispositivoCombo.addItem("SELECCIONAR")
		self.tipoDispositivoCombo.addItem("INTEL GALILEO")
		self.tipoDispositivoCombo.addItem("ESP32 IoT")
		self.tipoDispositivoCombo.addItem("RASPBERRY PI")

		self.generarButton.clicked.connect(self.generar)
		self.limpiarButton.clicked.connect(self.limpiar)

		self.setGeometry(490,150,310,350)
		self.setWindowTitle('Nuevo Dispositivo')
		self.setWindowIcon(QIcon('images/generador.png'))
		self.show()

	def mostrarVentana(self, evento):

		self.seleccionTarjeta = evento

		if evento == "INTEL GALILEO":
			self.nombreDispositivoLabel.move(20,70)
			self.ipDispositivoLabel.move(20,120)
			self.ipServidorLabel.move(20,170)
			self.macLabel.move(20,220)
			self.nombreRedLabel.move(500,500)
			self.contraRedLabel.move(500,500)

			self.nombreDispositivoLine.move(150,70)
			self.ipDispositivoLine.move(150,120)
			self.ipServidorLine.move(150,170)
			self.macLine.move(150,220)
			self.nombreRedLine.move(500,500)
			self.contraRedLine.move(500,500)
		elif evento == "ESP32 IoT":
			self.nombreDispositivoLabel.move(20,70)
			self.ipDispositivoLabel.move(20,120)
			self.ipServidorLabel.move(20,170)
			self.macLabel.move(500,500)
			self.nombreRedLabel.move(20,220)
			self.contraRedLabel.move(20,270)

			self.nombreDispositivoLine.move(150,70)
			self.ipDispositivoLine.move(150,120)
			self.ipServidorLine.move(150,170)
			self.macLine.move(500,500)
			self.nombreRedLine.move(150,220)
			self.contraRedLine.move(150,270)
		else:
			self.nombreDispositivoLabel.move(20,70)
			self.ipDispositivoLabel.move(20,120)
			self.ipServidorLabel.move(20,170)
			self.macLabel.move(500,500)
			self.nombreRedLabel.move(500,500)
			self.contraRedLabel.move(500,500)

			self.nombreDispositivoLine.move(150,70)
			self.ipDispositivoLine.move(150,120)
			self.ipServidorLine.move(150,170)
			self.macLine.move(500,500)
			self.nombreRedLine.move(500,500)
			self.contraRedLine.move(500,500)


	def tomarDatos(self):
		self.nombreDispositivo = self.nombreDispositivoLine.text()
		self.tipoDispositivo = self.tipoDispositivoCombo.currentText()
		self.ipDispositivo = self.ipDispositivoLine.text()
		self.ipServidor = self.ipServidorLine.text()
		self.mac = self.macLine.text()
		self.mac = self.mac.upper()
		self.nombreRed = self.nombreRedLine.text()
		self.contraRed = self.contraRedLine.text()

	def generar(self):

		x = "OK"

		self.tomarDatos()

		auxTam = len(self.nombreDispositivo)
		if auxTam == 0:
			x = "SELECCIONAR"

		if self.tipoDispositivo == "SELECCIONAR":
			x = "SELECCIONAR"

		auxTam = len(self.ipDispositivo)
		if auxTam == 0:
			x = "SELECCIONAR"

		auxTam = len(self.ipServidor)
		if auxTam == 0:
			x = "SELECCIONAR"

		if self.seleccionTarjeta == "INTEL GALILEO":
			auxTam = len(self.mac)
			if auxTam == 0:
				x = "SELECCIONAR"

		if self.seleccionTarjeta == "ESP32 IoT":
			auxTam = len(self.nombreRed)
			if auxTam == 0:
				x = "SELECCIONAR"

			auxTam = len(self.contraRed)
			if auxTam == 0:
				x = "SELECCIONAR"

		if x == "SELECCIONAR":
			error1 = "ERROR"
			error2 = "No se llenaron todos los campos\nIntente de nuevo."
			self.mensajeError(error1, error2)
		else:

			if self.tipoDispositivo == "INTEL GALILEO":
				self.galileo()
			elif self.tipoDispositivo == "ESP32 IoT":
				self.esp()
			elif self.tipoDispositivo == "RASPBERRY PI":
				self.raspberry()


	def galileo(self):


		self.archivoMuestra = open('muestraGalileo', 'r')
		self.archivoNuevo = open('export', 'r+')

		textoMuestra = self.archivoMuestra.read()

		self.archivoNuevo.write(textoMuestra)

		self.archivoNuevo.seek(72)
		self.archivoNuevo.write('byte mac[] = {0x' + self.mac[0:2] + ', 0x' + self.mac[2:4] + ', 0x' + self.mac[4:6] + ', 0x' + self.mac[6:8] + ', 0x' + self.mac[8:10] + ', 0x' + self.mac[10:12] + '}; //REVISAR MAC DE LA TARJETA')

		self.archivoNuevo.seek(197)
		self.archivoNuevo.write('IPAddress server (' + self.ipServidor + ');')

		self.archivoNuevo.seek(259)
		self.archivoNuevo.write('IPAddress direccion_ip_fija(' + self.ipDispositivo + '); // Dirección IP de Galileo')

		self.archivoNuevo.seek(1148)
		self.archivoNuevo.write('  client.print("NEW ' + self.nombreDispositivo + '");')

		self.archivoNuevo.seek(1583)
		self.archivoNuevo.write('    client.print("DIS ' + self.ipDispositivo + ' ' + self.nombreDispositivo + '");')

		self.archivoNuevo.seek(1715)
		self.archivoNuevo.write('    if(ipCadena == "' + self.ipDispositivo + '"){')

		self.archivoNuevo.seek(0)


		textoArchivo = self.archivoNuevo.read()
		self.showDialogSave(textoArchivo)

		self.archivoNuevo.close()
		self.archivoMuestra.close()

	def esp(self):

		self.archivoMuestra = open('muestraEsp', 'r')
		self.archivoNuevo = open('export', 'r+')

		textoMuestra = self.archivoMuestra.read()

		self.archivoNuevo.write(textoMuestra)

		
		self.archivoNuevo.seek(44)
		self.archivoNuevo.write('"' + self.nombreRed + '";')
		
		self.archivoNuevo.seek(97)
		self.archivoNuevo.write('"' + self.contraRed + '";')

		self.archivoNuevo.seek(150)
		self.archivoNuevo.write('"' + self.ipServidor + '";')		

		self.archivoNuevo.seek(201)
		self.archivoNuevo.write(self.ipDispositivo[0:3] + ', ' + self.ipDispositivo[4:7] + ', ' + self.ipDispositivo[8:9] + ', ' + self.ipDispositivo[10:12] + ');')

		self.archivoNuevo.seek(1564)
		self.archivoNuevo.write(self.nombreDispositivo + '");')

		self.archivoNuevo.seek(2041)
		self.archivoNuevo.write(self.ipDispositivo + ' ' + self.nombreDispositivo + '");')

		self.archivoNuevo.seek(2180)
		self.archivoNuevo.write(self.ipDispositivo + '"){')

		self.archivoNuevo.seek(0)

		textoArchivo = self.archivoNuevo.read()
		self.showDialogSave(textoArchivo)

		self.archivoNuevo.close()
		self.archivoMuestra.close()

	def raspberry(self):
		self.archivoMuestra = open('muestraRaspberry', 'r')
		self.archivoNuevo = open('export', 'r+')

		textoMuestra = self.archivoMuestra.read()

		self.archivoNuevo.write(textoMuestra)
		
		self.archivoNuevo.seek(130)
		self.archivoNuevo.write(self.ipServidor + "'")
		
		self.archivoNuevo.seek(256)
		self.archivoNuevo.write('NEW ' + self.nombreDispositivo + '"')

		self.archivoNuevo.seek(437)
		self.archivoNuevo.write(self.ipDispositivo + ' ' + self.nombreDispositivo + '"')

		self.archivoNuevo.seek(592)
		self.archivoNuevo.write(self.ipDispositivo + '":')

		self.archivoNuevo.seek(0)

		textoArchivo = self.archivoNuevo.read()
		self.showDialogSaveRasp(textoArchivo)

		self.archivoNuevo.close()
		self.archivoMuestra.close()
		
	def limpiar(self):
		self.nombreDispositivoLine.clear()
		self.ipDispositivoLine.clear()
		self.macLine.clear()
		self.ipServidorLine.clear()
		self.nombreRedLine.clear()
		self.contraRedLine.clear()

	
	def showDialogSave(self,cadena):
		
		archivo = self.fileDialog.getSaveFileName(self, "Generar Archivo", " ", "Arduino Files(*.ino)")

		if archivo[0]:
		
			f = open(archivo[0], 'w')

			f.write(cadena)
			f.close()

	def showDialogSaveRasp(self,cadena):
		
		archivo = self.fileDialog.getSaveFileName(self, "Generar Archivo", " ", "Python Files(*.py)")

		if archivo[0]:
		
			f = open(archivo[0], 'w')

			f.write(cadena)
			f.close()

	def mensajeError(self, error1, error2):

		message = QMessageBox(self)
		message.setIcon(QMessageBox.Information)
		message.setWindowModality(Qt.ApplicationModal)
		message.setWindowTitle("Domo")

		message.setText(error1)
		message.setInformativeText(error2)
		message.setStandardButtons(QMessageBox.Ok)
		message.open()
