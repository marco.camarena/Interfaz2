import socket
import threading

class Enviar(threading.Thread):

	def __init__(self, enviar, client):
		threading.Thread.__init__(self)
		self.enviar = enviar
		self.client = client
	def run(self):

		while True:
			try:
				if self.enviar.empty() == False:
					x = self.enviar.get()
					print("SEND: ", x)
					x = x + '\n'
					self.client.send(x.encode())
					if "bye\n" == x:
						print("Shutting down.")
						break

			except KeyboardInterrupt as k:
				print("Shutting down.")
				self.client.close()
				break