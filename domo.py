from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import sys, time, random

from evento import *
from play import *
from dibujo import *
from acerca import *
from generador import *

class Domo(QWidget):
	def __init__(self, enviar, dDispositivos, eventos, comandos, dispositivosIp, recibir):
		super().__init__()

		self.enviar = enviar
		self.dDispositivos = dDispositivos
		self.eventos = eventos
		self.comandos = comandos
		self.dispositivosIp = dispositivosIp
		self.recibir = recibir

		self.final = 0

		self.play = 0
		self.iniciado = False
		self.pausado = False
		self.detenido = True
		
		self.status = "1"   #1 = Vacio   -   2 = Editado   -   3 = Abierto
		self.proyectoActual = ""

		self.children = []
		self.initUI()

	def initUI(self):

		#SEPARADORES

		self.hbox = QHBoxLayout(self)

		self.top1 = QFrame(self)
		self.top1.setFrameShape(QFrame.StyledPanel)
		
		self.top2 = QScrollArea(self)
		self.top2.resize(1000,359)

		self.path = QPainterPath()
		self.dibujar = RenderArea(self.path, self.eventos)

		self.top2.setWidget(self.dibujar)

		self.bottomleft1 = QFrame(self)
		self.bottomleft1.setFrameShape(QFrame.StyledPanel)

		self.bottomleft2 = QFrame(self)
		self.bottomleft2.setFrameShape(QFrame.StyledPanel)

		self.bottomright = QFrame(self)
		self.bottomright.setFrameShape(QFrame.StyledPanel)

		self.splitter1 = QSplitter(Qt.Vertical)
		self.splitter1.addWidget(self.top1)
		self.splitter1.addWidget(self.top2)
		self.splitter1.setSizes([21,359])

		self.splitter2 = QSplitter(Qt.Horizontal)
		self.splitter2.addWidget(self.bottomleft1)
		self.splitter2.addWidget(self.bottomleft2)
		self.splitter2.addWidget(self.bottomright)
		self.splitter2.setSizes([210,310,480])

		self.splitter3 = QSplitter(Qt.Vertical)
		self.splitter3.addWidget(self.splitter1)
		self.splitter3.addWidget(self.splitter2)
		self.splitter3.setSizes([380,320])

		self.hbox.addWidget(self.splitter3)
		self.setLayout(self.hbox)

		#BOTONES 1

		self.verificarButton = QPushButton(self.bottomleft1)
		self.playButton = QPushButton(self.bottomleft1)
		self.pauseButton = QPushButton(self.bottomleft1)
		self.stopButton = QPushButton(self.bottomleft1)

		self.verificarButton.resize(85,85)
		self.playButton.resize(85,85)
		self.pauseButton.resize(85,85)
		self.stopButton.resize(85,85)

		self.verificarButton.move(10,10)
		self.playButton.move(1000,1000)
		self.pauseButton.move(10,105)
		self.stopButton.move(10,200)

		self.verificarButton.setIcon(QIcon("images/verificar.png"))
		self.playButton.setIcon(QIcon("images/play.png"))
		self.pauseButton.setIcon(QIcon("images/pause.png"))
		self.stopButton.setIcon(QIcon("images/stop.png"))

		self.verificarButton.setIconSize(QSize(80,80))
		self.playButton.setIconSize(QSize(80,80))
		self.pauseButton.setIconSize(QSize(80,80))
		self.stopButton.setIconSize(QSize(80,80))

		self.verificarButton.clicked.connect(self.verificarButtonEvent)
		self.playButton.clicked.connect(self.playButtonEvent)
		self.pauseButton.clicked.connect(self.pauseButtonEvent)
		self.stopButton.clicked.connect(self.stopButtonEvent)


		#BOTONES 2

		self.agregarButton = QPushButton("AGREGAR", self.bottomleft1)
		self.modificarButton = QPushButton("MODIFICAR", self.bottomleft1)
		self.eliminarButton = QPushButton("ELIMINAR", self.bottomleft1)

		self.agregarButton.resize(85,85)
		self.modificarButton.resize(85,85)
		self.eliminarButton.resize(85,85)

		self.agregarButton.move(105,10)
		self.modificarButton.move(105,105)
		self.eliminarButton.move(105,200)

		self.agregarButton.clicked.connect(self.nuevoEvento)
		self.modificarButton.clicked.connect(self.modificarEvento)
		self.eliminarButton.clicked.connect(self.eliminarEvento)

		#AREA 3

		self.comandosEdit = QTextEdit()
		self.comandosEdit.setReadOnly(True)

		self.comandosBoxH = QHBoxLayout()
		self.comandosBoxH.addWidget(self.comandosEdit)

		self.comandosBoxV = QVBoxLayout(self.bottomright)
		self.comandosBoxV.addLayout(self.comandosBoxH)

		#VENTANA EDITAR EVENTOS

		self.agregarLabel = QLabel("AGREGAR EVENTO", self.bottomleft2)		
		self.modificarLabel = QLabel("MODIFICAR EVENTO", self.bottomleft2)
		self.eliminarLabel = QLabel("ELIMINAR EVENTO", self.bottomleft2)

		self.eventoLabel = QLabel("EVENTO", self.bottomleft2)
		self.dispositivoLabel = QLabel("DISPOSITIVO", self.bottomleft2)
		self.inicioLabel = QLabel("INICIO", self.bottomleft2)
		self.finLabel = QLabel("FIN", self.bottomleft2)
		self.intensidadLabel = QLabel("INTENSIDAD", self.bottomleft2)

		self.eventoCombo = QComboBox(self.bottomleft2)
		self.eventoEdit = QLineEdit(self.bottomleft2)

		self.eventoCombo.activated[str].connect(self.mostrarValoresActualesModificarEliminar)

		self.dispositivoCombo = QComboBox(self.bottomleft2)
		self.dispositivoMostrarValor = QLabel(self.bottomleft2)

		#QSPIN PARA EL TIEMPO DE INICIO DE EVENTO
		self.inicioHH = QLabel("HH", self.bottomleft2)
		self.inicioMM = QLabel("MM", self.bottomleft2)
		self.inicioSS = QLabel("SS", self.bottomleft2)
		self.inicioMostrarValor = QLabel(self.bottomleft2)

		self.inicioHora = QSpinBox(self.bottomleft2)
		self.inicioMinuto = QSpinBox(self.bottomleft2)
		self.inicioSegundo = QSpinBox(self.bottomleft2)

		self.inicioHora.setMaximum(1)
		self.inicioMinuto.setMaximum(59)
		self.inicioSegundo.setMaximum(59)

		#QSPIN PARA EL TIEMPO DE FIN DE EVENTO

		self.finHH = QLabel("HH", self.bottomleft2)
		self.finMM = QLabel("MM", self.bottomleft2)
		self.finSS = QLabel("SS", self.bottomleft2)
		self.finMostrarValor = QLabel(self.bottomleft2)

		self.finHora = QSpinBox(self.bottomleft2)
		self.finMinuto = QSpinBox(self.bottomleft2)
		self.finSegundo = QSpinBox(self.bottomleft2)

		self.finHora.setMaximum(1)
		self.finMinuto.setMaximum(59)
		self.finSegundo.setMaximum(59)
		

		self.intensidadCombo = QComboBox(self.bottomleft2)
		self.intensidadMostrarValor = QLabel(self.bottomleft2)

		self.eventoCombo.resize(160,25)
		self.eventoEdit.resize(160,25)
		self.dispositivoCombo.resize(160,25)
		self.dispositivoMostrarValor.resize(160,25)
		self.inicioMostrarValor.resize(160,25)
		self.finMostrarValor.resize(160,25)
		self.intensidadCombo.resize(160,25)
		self.intensidadMostrarValor.resize(160,25)

		self.aceptarButton = QPushButton("ACEPTAR", self.bottomleft2)
		self.cancelarButton = QPushButton("CANCELAR", self.bottomleft2)
		self.aceptarButton.clicked.connect(self.aceptarButtonEvent)
		self.cancelarButton.clicked.connect(self.cancelarButtonEvent)

		#BARRA DE MENU

		self.fileDialog = QFileDialog()

		menubar = QMenuBar(self.top1)
		menuArchivo = menubar.addMenu('Archivo')
		menuDispositivos = menubar.addMenu('Dispositivos')
		menuAyuda = menubar.addMenu('Ayuda')

		#MENU ARCHIVO
		newFile = QAction(QIcon('images/nuevo.png'), 'Nuevo', self)
		newFile.setShortcut('Ctrl+N')
		newFile.triggered.connect(self.nuevo)

		openFile = QAction(QIcon('images/abrir.png'), 'Abrir', self)
		openFile.setShortcut('Ctrl+O')
		openFile.triggered.connect(self.abrir)

		saveFile = QAction(QIcon('images/guardar.png'), 'Guardar', self)
		saveFile.setShortcut('Ctrl+S')
		saveFile.triggered.connect(self.showDialogSave)

		saveFileAs = QAction(QIcon('images/guardar.png'), 'Guardar como', self)
		saveFileAs.setShortcut('Ctrl+Shift+S')
		saveFileAs.triggered.connect(self.showDialogSaveAs)

		menuArchivo.addAction(newFile)
		menuArchivo.addAction(openFile)
		menuArchivo.addAction(saveFile)
		menuArchivo.addAction(saveFileAs)

		#MENU DISPOSITIVOS
		addDispGalileo = QAction(QIcon('images/add.png'), 'Nuevo Dispositivo', self)
		addDispGalileo.triggered.connect(self.addDispositivo)

		menuDispositivos.addAction(addDispGalileo)


		#MENU ACERCA DE
		acercaDe = QAction(QIcon('images/acerca.png'), 'Acerca de', self)
		acercaDe.triggered.connect(self.acercaDe)

		menuAyuda.addAction(acercaDe)


		self.comandosEdit.insertPlainText("BIENVENIDO")
		
		self.esconderVentana()
		self.setGeometry(170,50,1000,700)
		self.setWindowTitle('Domo')
		self.setWindowIcon(QIcon('images/domo.png'))
		self.show()

	#DIALOGS ABRIR, GUARDAR

	def nuevo(self):
		if self.status == "1":
			pass
		else:
			error1 = "Desea guardar el proyecto actual"
			error2 = "Si elige No se perdera el avance"
			
			error = self.mensajeError(error1, error2, "2")

			if error == "Yes":
				self.showDialogSave()
			else:
				pass

			self.limpiarProyecto()
			self.status = "1"


	def limpiarProyecto(self):
		self.esconderVentana()
		self.limpiarCampos()
		self.limpiarTiempo()

		self.comandosEdit.clear()
		self.eventos.clear()
		self.comandosEdit.insertPlainText("BIENVENIDO")

	def abrir(self):

		if self.status == "1":
			self.showDialogOpen()
			self.status = "3"

		else:
			error1 = "Desea guardar el proyecto actual"
			error2 = "Si elige No se perdera el avance"
			
			error = self.mensajeError(error1, error2, "2")

			if error == "Yes":
				self.showDialogSave()
			else:
				pass

			self.showDialogOpen()
			self.status = "3"


	def showDialogOpen(self):
		archivo = self.fileDialog.getOpenFileName(self, "Abrir Proyecto", " ", "Domo Files(*.domo)")

		if archivo[0]:
			f = open(archivo[0], 'r')

			with f:
				self.archivoAbierto = f.read()
				self.importarEventos(self.archivoAbierto)

				info = "SE ABRIO PROYECTO CORRECTAMENTE"
				self.ventanaInfo(info)

			f.close()

	def showDialogSave(self):
		
		archivo = self.fileDialog.getSaveFileName(self, "Guardar", " ", "Domo Files(*.domo)")

		cadena = self.exportarEventos()

		if archivo[0]:
		
			f = open(archivo[0], 'w')

			f.write(cadena)
			f.close()

	def showDialogSaveAs(self):
		archivo = self.fileDialog.getSaveFileName(self, "Guardar Como", " ", "Domo Files(*.domo)")

		cadena = self.exportarEventos()

		if archivo[0]:
		
			f = open(archivo[0], 'w')
			f.write(cadena)

			info = "SE GUARDO PROYECTO CORRECTAMENTE"
			self.ventanaInfo(info)

			f.close()

	def importarEventos(self, archivo):

		self.eventos.clear()

		tam = len(archivo)
		x = 0
		dato = 0

		palabraActual = ""
		caracterActual = ""

		while True:
			if x == tam:
				break

			caracterActual = archivo[x]

			if caracterActual == "#":
				dato = dato + 1
				x = x + 1

				if dato == 1:
					self.nombreEvento = palabraActual
					palabraActual = ""
				elif dato == 2:
					self.nombreDispositivo = palabraActual
					palabraActual = ""
				elif dato == 3:
					self.ip = palabraActual
					palabraActual = ""
				elif dato == 4:
					self.inicio = palabraActual
					palabraActual = ""
				elif dato == 5:
					self.fin = palabraActual
					palabraActual = ""

					if self.final < int(self.fin):
						self.final = int(self.fin)
				
			elif caracterActual == "\n":
				dato = 0
				x = x + 1

				self.intensidad = palabraActual
				palabraActual = ""

				evento = Evento(self.nombreEvento, self.nombreDispositivo, self.ip, int(self.inicio), int(self.fin), self.intensidad)
				self.eventos.append(evento)
			else:
				palabraActual = palabraActual + caracterActual
				x = x + 1

	def exportarEventos(self):

		cadena = ""

		for c in self.eventos:
			cadena = cadena + c.getNombreEvento() + "#"
			cadena = cadena + c.getNombreDispositivo() + "#"
			cadena = cadena + c.getIp() + "#"
			cadena = cadena + str(c.getInicio()) + "#"
			cadena = cadena + str(c.getFin()) + "#"
			cadena = cadena + c.getIntensidad() + "\n"

		return cadena

	def addDispositivo(self):
		generadorCodigo = Generador()
		self.children.append(generadorCodigo)

	def acercaDe(self):
		acerca = Acerca()
		self.children.append(acerca)

	#EVENTOS BOTONES VERIFICAR, PLAY, PAUSE, STOP

	def verificarButtonEvent(self):

		verificacionCorrecta = True

		self.enviar.put("DD")

		info = "VERIFICANDO EVENTOS Y CONEXION DE DISPOSITIVOS"
		self.ventanaInfo(info)

		time.sleep(0.6)

		listaDisp = {}
		numDisp = 0
		dispositivoFaltante = ""

		while(True):
			if self.dDispositivos.empty() == False:
				aux = self.dDispositivos.get()

				nombre = aux[13:]
				tam = len(nombre)
				nombre = nombre[:tam-1]

				listaDisp[numDisp] = nombre
				numDisp = numDisp + 1

			else:
				break

		numEventos = 0
		for f in self.eventos:
			numEventos = numEventos + 1

		if numEventos > 0:

			for e in self.eventos:
				for d in range(0,numDisp):

					auxDisp = listaDisp.get(d)

					if e.getNombreDispositivo() == auxDisp:
						verificacionCorrecta = True
						break
					else:
						verificacionCorrecta = False
						dispositivoFaltante = str(e.getNombreDispositivo())

				if verificacionCorrecta == False:
					break

			if verificacionCorrecta == True:
				self.verificarButton.move(1000,1000)
				self.playButton.move(10,10)

				info = "VERIFICACION CORRECTA"
				self.ventanaInfo(info)

			else:
				info = "VERIFICACION FALLIDA\nNO SE ENCONTRO EL SIGUIENTE DISPOSITIVO:\n" + dispositivoFaltante
				self.ventanaInfo(info)
		else:
			info = "VERIFICACION FALLIDA\nNO SE ENCONTRARON EVENTOS"
			self.ventanaInfo(info)

	def playButtonEvent(self):

		print(self.iniciado)

		if self.iniciado == False:
			info = "SE INICIO EL ENVIO DE COMANDOS"
			self.ventanaInfo(info)

			self.play = Play(self.eventos, self.enviar, self.final, self.comandosEdit)
			self.play.start()
			self.iniciado = True
			self.detenido = False

			self.esconderVentana()
			self.limpiarCampos()

		else:

			if self.play.stoped == True:
				self.iniciado = False
				self.pausado = False
				self.detenido = True

				self.playButtonEvent()

			if self.pausado == True:
				info = "SE REANUDO EL ENVIO DE COMANDOS"
				self.ventanaInfo(info)
				self.pausado = False
				self.play.resume()

	def pauseButtonEvent(self):

		if self.iniciado == True:
			if self.pausado == False:
				info = "SE PAUSO EL ENVIO DE COMANDOS"
				self.ventanaInfo(info)
				self.pausado = True
				self.play.pause()

	def stopButtonEvent(self):

		if self.play.stoped == True:
			self.iniciado = False
			self.pausado = False
			self.detenido = True

		if self.iniciado == True:
			if self.detenido == False:
				info = "SE DETUVO EL ENVIO DE COMANDOS"
				self.ventanaInfo(info)
				self.iniciado = False
				self.pausado = False
				self.detenido = True
				self.play.stop()

	#EVENTOS NUEVO, MODIFICAR, ELIMINAR

	def nuevoEvento(self):

		if self.iniciado == False:
			self.status = '2'
			self.esconderVentana()	
			self.limpiarCampos()
			self.limpiarDispositivos()
			self.agregarLabel.move(70,20)
			self.accion = "NUEVO"

			self.enviar.put("DD")
			self.mostrarVentana()
			time.sleep(0.6)
			self.mostrarDispositivos()

		else:
			error1 = "ERROR"
			error2 = "No se puede agregar eventos mientras se esta reproduciendo."
			self.mensajeError(error1, error2, "1")

	def modificarEvento(self):

		if self.iniciado == False:

			self.esconderVentana()
			self.limpiarCampos()
			self.limpiarDispositivos()
			self.modificarLabel.move(70,20)
			self.accion = "MODIFICAR"

			self.enviar.put("DD")
			self.mostrarVentana()
			time.sleep(0.6)
			self.mostrarEventos()

		else:
			error1 = "ERROR"
			error2 = "No se puede modificar eventos mientras se esta reproduciendo."
			self.mensajeError(error1, error2, "1")

	def eliminarEvento(self):

		if self.iniciado == False:

			self.esconderVentana()
			self.limpiarCampos()
			self.limpiarDispositivos()
			self.eliminarLabel.move(70,20)
			self.accion = "ELIMINAR"

			self.mostrarVentana()
			self.mostrarEventos()

		else:
			error1 = "ERROR"
			error2 = "No se puede eliminar eventos mientras se esta reproduciendo."
			self.mensajeError(error1, error2, "1")




	def aceptarButtonEvent(self):

		continuaAccion = False

		if self.accion == "NUEVO":

			nombreEventoEdit = self.eventoEdit.text()
			x = self.tomarValores()

			#SE COMPRUEBA SI EL USUARIO INGRESO NOMBRE DE EVENTO

			tamNombre = len(nombreEventoEdit)
			if tamNombre == 0:
				x = "SELECCIONAR"

			#SE REVISAN LOS EVENTOS PARA SABER SI EL NOMBRE DEL EVENTO ACTUAL NO EXISTE

			for c in self.eventos:
				if c.getNombreEvento() == nombreEventoEdit:
					x = "EVENTO"


			if x == "OK":

				if self.final < self.fin:
					self.final = self.fin

				if self.inicio >= self.fin:
					error1 = "ERROR DE LLENADO"
					error2 = "El tiempo de inicio debe ser menor al tiempo de fin\nIntente de nuevo."
					self.mensajeError(error1, error2, "1")
					continuaAccion = False

				else:
					evento = Evento(nombreEventoEdit, self.nombreDispositivo, self.ip, self.inicio, self.fin, self.intensidad)
					self.eventos.append(evento)

					info = "SE AGREGO EVENTO:\n Nombre       - " + nombreEventoEdit + "\n Dispositivo  - " + self.nombreDispositivo + "\n Direccion IP - " + self.ip + "\n Inicio       - " + str(self.inicio) + "\n Fin          - " + str(self.fin) + "\n Intensidad   - " + self.intensidad
					self.ventanaInfo(info)

					continuaAccion = True

			elif x == "EVENTO":
				error1 = "ERROR DE LLENADO"
				error2 = "El nombre del evento ya existe\nIntente de nuevo."
				self.mensajeError(error1, error2, "1")

				info = "ERROR AL AGREGAR NUEVO EVENTO"
				self.ventanaInfo(info)

				continuaAccion = False

			elif x == "SELECCIONAR":
				error1 = "ERROR DE LLENADO"
				error2 = "No se llenaron todos los campos\nIntente de nuevo."
				self.mensajeError(error1, error2, "1")

				info = "ERROR AL AGREGAR NUEVO EVENTO"
				self.ventanaInfo(info)

				continuaAccion = False

			elif x == "TIEMPOS":
				error1 = "ERROR DE LLENADO"
				error2 = "El tiempo de este evento se cruza con otro ya existente\nIntente de nuevo."
				self.mensajeError(error1, error2, "1")

				info = "ERROR AL AGREGAR NUEVO EVENTO"
				self.ventanaInfo(info)

				continuaAccion = False

		elif self.accion == "MODIFICAR":

			nombreEventoCombo = self.eventoCombo.currentText()

			if nombreEventoCombo == "SELECCIONAR":
				x = "EVENTO"
			else:
				x = self.tomarValores()

			if x == "OK":

				if self.final < self.fin:
					self.final = self.fin

				if self.inicio >= self.fin:
					error1 = "ERROR EN TIEMPO DE INICIO Y FIN"
					error2 = "El tiempo de inicio debe ser menor al tiempo de fin\nIntente de nuevo."
					self.mensajeError(error1, error2, "1")
					continuaAccion = False
				else:
					for c in self.eventos:
						if c.getNombreEvento() == nombreEventoCombo:
							c.cambiarDispositivo(self.nombreDispositivo)
							c.cambiarIp(self.ip)
							c.cambiarInicio(self.inicio)
							c.cambiarFin(self.fin)
							c.cambiarIntensidad(self.intensidad)

					info = "SE MODIFICO EVENTO:\n Nombre       - " + nombreEventoCombo + "\n Dispositivo  - " + self.nombreDispositivo + "\n Direccion IP - " + self.ip + "\n Inicio       - " + str(self.inicio) + "\n Fin          - " + str(self.fin) + "\n Intensidad   - " + self.intensidad
					self.ventanaInfo(info)

					continuaAccion = True

			elif x == "EVENTO":
				error1 = "ERROR AL MODIFICAR"
				error2 = "No selecciono ningun evento a modificar\nIntente de nuevo."
				self.mensajeError(error1, error2, "1")

				info = "ERROR AL MODIFICAR EVENTO"
				self.ventanaInfo(info)

				continuaAccion = False

			elif x == "SELECCIONAR":
				error1 = "ERROR DE LLENADO"
				error2 = "No se llenaron todos los campos\nIntente de nuevo."
				self.mensajeError(error1, error2, "1")

				info = "ERROR AL MODIFICAR EVENTO"
				self.ventanaInfo(info)

				continuaAccion = False

			elif x == "TIEMPOS":
				error1 = "ERROR DE LLENADO"
				error2 = "El tiempo de este evento se cruza con otro ya existente\nIntente de nuevo."
				self.mensajeError(error1, error2, "1")

				info = "ERROR AL MODIFICAR EVENTO"
				self.ventanaInfo(info)

				continuaAccion = False


		elif self.accion == "ELIMINAR":

			nombreEventoCombo = self.eventoCombo.currentText()

			if nombreEventoCombo == "SELECCIONAR":
				error1 = "ERROR AL ELIMINAR"
				error2 = "No selecciono ningun evento a eliminar\nIntente de nuevo."
				self.mensajeError(error1, error2, "1")

				info = "ERROR AL ELIMINAR EVENTO"
				self.ventanaInfo(info)

				continuaAccion = False
			else:
				i = 0
				for c in self.eventos:
					if c.getNombreEvento() == nombreEventoCombo:
						self.eventos.remove(c)
						continuaAccion = True
					i = i + 1

				info = "SE ELIMINO EVENTO:\n Nombre       - " + nombreEventoCombo
				self.ventanaInfo(info)


		if continuaAccion == True:
			self.verificarButton.move(10,10)
			self.playButton.move(1000,1000)
			self.dibujar.actualizarPainter()
			self.esconderVentana()
			self.limpiarCampos()
		else:
			pass

	def mensajeError(self, error1, error2, tipo):

		message = QMessageBox(self)
		message.setIcon(QMessageBox.Information)
		message.setWindowModality(Qt.ApplicationModal)
		message.setWindowTitle("Domo")

		if tipo == "1":
			message.setText(error1)
			message.setInformativeText(error2)
			message.setStandardButtons(QMessageBox.Ok)
			message.open()

		elif tipo == "2":
			message.setText(error1)
			message.setInformativeText(error2)
			message.setStandardButtons(QMessageBox.Yes)
			message.addButton(QMessageBox.No)
			message.setDefaultButton(QMessageBox.No)
			message.open()

			if message.exec() == QMessageBox.Yes:
				return "Yes"
			else:
				return "No"

	def tomarValores(self):
		self.nombreDispositivo = self.dispositivoCombo.currentText()

		if self.nombreDispositivo == "SELECCIONAR":
			return "SELECCIONAR"
		else:
			tamNombre = len(self.nombreDispositivo)
			if tamNombre == 0:
				return "SELECCIONAR"

		self.ip = self.dispositivosIp[self.nombreDispositivo]
		
		self.horaInicio = self.inicioHora.value()
		self.minutoInicio = self.inicioMinuto.value()
		self.segundoInicio = self.inicioSegundo.value()

		self.horaFin = self.finHora.value()
		self.minutoFin = self.finMinuto.value()
		self.segundoFin = self.finSegundo.value()

		self.inicio = (self.horaInicio * 60 * 60) + (self.minutoInicio * 60) + (self.segundoInicio)
		self.fin = (self.horaFin * 60 *60) + (self.minutoFin * 60) + (self.segundoFin)

		for c in self.eventos:

			auxNombre = c.getNombreDispositivo()

			if auxNombre == self.nombreDispositivo:
				if self.inicio >= c.getInicio():
					if self.inicio < c.getFin():
						return "TIEMPOS"
				else:
					if self.fin > c.getInicio():
						return "TIEMPOS"


		self.intensidad = self.intensidadCombo.currentText()

		if self.intensidad == "SELECCIONAR":
			return "SELECCIONAR"

		return "OK"


	def cancelarButtonEvent(self):
		self.esconderVentana()
		self.limpiarCampos()

		#for c in self.eventos:
		#	print(" ")
		#	print(c.getNombreEvento())
		#	print(c.getNombreDispositivo())
		#	print(c.getIp())
		#	print(c.getInicio())
		#	print(c.getFin())
		#	print(c.getIntensidad())
		#	print(" ")

	def ventanaInfo(self, mensaje):
		actual = ""
		nuevo = ""

		actual = self.comandosEdit.toPlainText()
		self.comandosEdit.clear()

		self.comandosEdit.insertPlainText(mensaje + "\n-------------------------------------------\n" + actual)

	def mostrarVentana(self):

		self.eventoLabel.move(10,60)
		self.dispositivoLabel.move(10,100)
		self.inicioLabel.move(10,140)
		self.finLabel.move(10,180)
		self.intensidadLabel.move(10,220)

		self.cancelarButton.move(100,270)
		self.aceptarButton.move(200,270)

		if self.accion == "NUEVO":
			self.widgetsAgregarModificar()
			self.eventoEdit.move(130,60)

			self.eventoEdit.setPlaceholderText("INGRESE NOMBRE")
			self.dispositivoCombo.addItem("SELECCIONAR")

			self.intensidadCombo.addItem("SELECCIONAR")
			self.intensidadCombo.addItem("ALTA")
			self.intensidadCombo.addItem("MEDIA")
			self.intensidadCombo.addItem("BAJA")

		elif self.accion == "MODIFICAR":
			self.widgetsAgregarModificar()
			self.eventoCombo.move(130,60)
			self.eventoCombo.addItem("SELECCIONAR")

		elif self.accion == "ELIMINAR":
			self.eventoCombo.move(130,60)
			self.eventoCombo.addItem("SELECCIONAR")

			self.dispositivoMostrarValor.move(130,95)
			self.inicioMostrarValor.move(130,135)
			self.finMostrarValor.move(130,175)
			self.intensidadMostrarValor.move(130,215)

	def widgetsAgregarModificar(self):
		self.dispositivoCombo.move(130,100)

		self.inicioHH.move(115,143)
		self.inicioMM.move(174,143)
		self.inicioSS.move(235,143)

		self.finHH.move(115,183)
		self.finMM.move(174,183)
		self.finSS.move(235,183)

		self.inicioHora.move(130,140)
		self.inicioMinuto.move(190,140)
		self.inicioSegundo.move(250,140)

		self.finHora.move(130, 180)
		self.finMinuto.move(190,180)
		self.finSegundo.move(250,180)

		self.intensidadCombo.move(130,220)

	def esconderVentana(self):
		self.agregarLabel.move(1000,700)
		self.modificarLabel.move(1000,700)
		self.eliminarLabel.move(1000,700)

		self.eventoLabel.move(1000,700)
		self.dispositivoLabel.move(1000,700)
		self.inicioLabel.move(1000,700)
		self.finLabel.move(1000,700)
		self.intensidadLabel.move(1000,700)

		self.eventoCombo.move(1000,700)
		self.eventoEdit.move(1000,700)
		self.dispositivoCombo.move(1000,700)
		self.dispositivoMostrarValor.move(1000,700)
		self.inicioMostrarValor.move(1000,700)
		self.finMostrarValor.move(1000,700)

		self.inicioHH.move(1000,700)
		self.inicioMM.move(1000,700)
		self.inicioSS.move(1000,700)

		self.finHH.move(1000,700)
		self.finMM.move(1000,700)
		self.finSS.move(1000,700)

		self.inicioHora.move(1000,700)
		self.inicioMinuto.move(1000,700)
		self.inicioSegundo.move(1000,700)

		self.finHora.move(1000,700)
		self.finMinuto.move(1000,700)
		self.finSegundo.move(1000,700)

		self.intensidadCombo.move(1000,700)
		self.intensidadMostrarValor.move(1000,700)

		self.cancelarButton.move(1000,700)
		self.aceptarButton.move(1000,700)

	def mostrarDispositivos(self):
		while(True):
			if self.dDispositivos.empty() == False:
				aux = self.dDispositivos.get()

				nombre = aux[13:]
				tam = len(nombre)
				nombre = nombre[:tam-1]
				ip = aux[0:12]

				self.dispositivosIp[nombre] = ip
				self.dispositivoCombo.addItem(nombre)

			else:
				break

	def limpiarDispositivos(self):
		while(True):
			if self.dDispositivos.empty() == False:
				self.dDispositivos.get()

			else:
				break

	def mostrarEventos(self):
		for c in self.eventos:
			self.eventoCombo.addItem(c.getNombreEvento())

	def mostrarValoresActualesModificarEliminar(self, evento):
		self.limpiarTiempo()

		tiempoInicio = 0
		tiempoFin = 0

		for c in self.eventos:
			if c.getNombreEvento() == evento:
				tiempoInicio = c.getInicio()
				tiempoFin = c.getFin()

				self.conversionMinutos(tiempoInicio, tiempoFin)


				if self.accion == "MODIFICAR":
					self.mostrarDispositivos()
				
					self.inicioHora.setValue(self.horaInicio)
					self.inicioMinuto.setValue(self.minutoInicio)
					self.inicioSegundo.setValue(self.segundoInicio)

					self.finHora.setValue(self.horaFin)
					self.finMinuto.setValue(self.minutoFin)
					self.finSegundo.setValue(self.segundoFin)


					self.intensidadCombo.addItem("ALTA")
					self.intensidadCombo.addItem("MEDIA")
					self.intensidadCombo.addItem("BAJA")

				elif self.accion == "ELIMINAR":

					if self.horaInicio < 10:
						self.horaInicio = "0" + str(self.horaInicio)
					if self.minutoInicio < 10:
						self.minutoInicio = "0" + str(self.minutoInicio)
					if self.segundoInicio < 10:
						self.segundoInicio = "0" + str(self.segundoInicio)

					if self.horaFin < 10:
						self.horaFin = "0" + str(self.horaFin)
					if self.minutoFin < 10:
						self.minutoFin = "0" + str(self.minutoFin)
					if self.segundoFin < 10:
						self.segundoFin = "0" + str(self.segundoFin)

					self.dispositivoMostrarValor.setText(c.getNombreDispositivo())
					self.inicioMostrarValor.setText(str(self.horaInicio) + " : " + str(self.minutoInicio) + " : " + str(self.segundoInicio))
					self.finMostrarValor.setText(str(self.horaFin) + " : " + str(self.minutoFin) + " : " + str(self.segundoFin))
					self.intensidadMostrarValor.setText(c.getIntensidad())

	def conversionMinutos(self, tiempoInicio, tiempoFin):

		self.horaInicio = 0
		self.minutoInicio = 0
		self.segundoInicio = 0

		self.horaFin = 0
		self.minutoFin = 0
		self.segundoFin = 0

		self.horaInicio = int(tiempoInicio) // 3600
		if self.horaInicio >= 1:
			tiempoInicio = int(tiempoInicio) - (self.horaInicio * 3600)

		self.minutoInicio = int(tiempoInicio) // 60
		if self.minutoInicio >= 1:
			tiempoInicio = int(tiempoInicio) - (self.minutoInicio * 60)

		self.segundoInicio = int(tiempoInicio)


		self.horaFin = int(tiempoFin) // 3600
		if self.horaFin >= 1:
			tiempoFin = int(tiempoFin) - (self.horaFin * 3600)	

		self.minutoFin = int(tiempoFin) // 60
		if self.minutoFin >= 1:
			tiempoFin = int(tiempoFin) - (self.minutoFin * 60)

		self.segundoFin = int(tiempoFin)
		

	def limpiarCampos(self):
		self.eventoCombo.clear()
		self.dispositivoCombo.clear()
		self.intensidadCombo.clear()
		self.eventoEdit.clear()
		self.dispositivoMostrarValor.clear()
		self.inicioMostrarValor.clear()
		self.finMostrarValor.clear()
		self.intensidadMostrarValor.clear()
		self.limpiarTiempo()
		

	def limpiarTiempo(self):
		self.inicioHora.clear()
		self.inicioMinuto.clear()
		self.inicioSegundo.clear()

		self.finHora.clear()
		self.finMinuto.clear()
		self.finSegundo.clear()